const INVALID_VALUE = "Invalid";

function getVaildID(id) {
  if (
    id !== undefined &&
    id !== null &&
    id.length !== 13 &&
    !/^\d+$/.test(id)
  ) {
    console.log("daaa");
    return INVALID_VALUE;
  } else if (id === undefined || id === null) {
    console.log("nuuuu");
    return INVALID_VALUE;
  }
  const sum =
    id.charAt(0) * 2 +
    id.charAt(1) * 7 +
    id.charAt(2) * 9 +
    id.charAt(3) * 1 +
    id.charAt(4) * 4 +
    id.charAt(5) * 6 +
    id.charAt(6) * 3 +
    id.charAt(7) * 5 +
    id.charAt(8) * 8 +
    id.charAt(9) * 2 +
    id.charAt(10) * 7 +
    id.charAt(11) * 9;
  if (sum % 11 < 10 && sum % 11 === id[12]) {
    return id;
  } else {
    console.log("poateee");
    return INVALID_VALUE;
  }
}

function getValidBloodType(bloodTypeString) {
  if (bloodTypeString === null || bloodTypeString === undefined) {
    return INVALID_VALUE;
  }
  bloodTypeString = bloodTypeString.toUpperCase();
  const possibleBloodTypes = [
      "A+",
      "A-",
      "B+",
      "B-",
      "O+",
      "O-",
      "AB+",
      "AB-",
      "A",
      "B",
      "O",
      "AB",
    ],
    matchingBloodType = possibleBloodTypes.find((e) => e === bloodTypeString);
  if (matchingBloodType) {
    return matchingBloodType;
  } else {
    return INVALID_VALUE;
  }
}

export function correctMistakes(data) {
  //data.cnp = getVaildID(data.cnp);
  data.bloodType = getValidBloodType(data.bloodType);
  const mistakesFound =
    data.cnp === INVALID_VALUE || data.bloodType === INVALID_VALUE;
  console.log(data);
  return {
    mistakesFound: mistakesFound,
    correctDara: data,
  };
}
