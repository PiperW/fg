import blue from "@material-ui/core/colors/blue";
import pink from "@material-ui/core/colors/pink";

export default {
  palette: {
    primary: blue,
    secondary: pink
  },
  typography: {
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"'
    ].join(","),
    useNextVariants: true
  },
  form: {
    textAlign: "center"
  },
  image: {
    margin: "10px auto 0px auto"
  },
  pageTitle: {
    margin: "0px auto 10px auto"
  },
  textField: {
    margin: "10px auto 10px auto"
  },
  button: {
    marginTop: 20,
    position: "relative"
  },
  customError: {
    color: "red",
    fontSize: "0.8rem",
    marginTop: 10
  },
  progress: {
    position: "absolute"
  },
  goToLink: {
    marginTop: 10
  }
};
