import React from "react";
import { Route, Redirect } from "react-router-dom";
const HomeAuthRoute = ({
  component: Component,
  authenticated,
  updateAuthStatus,
  redirectPath,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) =>
      authenticated === false ? (
        <Redirect to={redirectPath} />
      ) : (
        <Component {...props} updateAuthStatus={updateAuthStatus} />
      )
    }
  />
);

export default HomeAuthRoute;
