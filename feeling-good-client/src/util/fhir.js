const accessToken =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkN0VHVoTUptRDVNN0RMZHpEMnYyeDNRS1NSWSIsImtpZCI6IkN0VHVoTUptRDVNN0RMZHpEMnYyeDNRS1NSWSJ9.eyJhdWQiOiJodHRwczovL3BvcnRhYmxlLmF6dXJlaGVhbHRoY2FyZWFwaXMuY29tIiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvNmJiNDFmZTQtNDBhMy00YTEwLWI2Y2QtMzgyNzhlNzhiMjFhLyIsImlhdCI6MTU4OTM4NjY0NSwibmJmIjoxNTg5Mzg2NjQ1LCJleHAiOjE1ODkzOTA1NDUsImFjciI6IjEiLCJhaW8iOiJBU1FBMi84UEFBQUFjbmQrNk9LOGRBUnkyc1NxdHpWWHlhcVowWG5mM1hCT2hKVkt4RDRjM2RjPSIsImFtciI6WyJwd2QiXSwiYXBwaWQiOiJmOWI5MzkwNi0yNmIwLTQyNmEtOWViNC1kMzQ4MDhiMzkyZjQiLCJhcHBpZGFjciI6IjAiLCJmYW1pbHlfbmFtZSI6IkNpdWx1IiwiZ2l2ZW5fbmFtZSI6Ik1pcnVuYSIsImlwYWRkciI6Ijg5LjEzNi4zNS4xMzEiLCJuYW1lIjoiTWlydW5hIENpdWx1Iiwib2lkIjoiNjhhZDliZGMtMTgzOS00OGE1LWE0NjctOTkwY2I4ZTc5ZGQ5Iiwib25wcmVtX3NpZCI6IlMtMS01LTIxLTk0MTg3OTU0Ni0xMTg5ODIwOTYxLTI2MzMyNjQxODQtMTc4NDYiLCJwdWlkIjoiMTAwMzAwMDA5M0ZFOEJFMiIsInNjcCI6InVzZXJfaW1wZXJzb25hdGlvbiIsInN1YiI6Ikl1eUV4YnZzZDFNZ3NHOEtJUHZ2Q0RRc1gtMWh2T3gxVU5IMkEzTnJVRmciLCJ0aWQiOiI2YmI0MWZlNC00MGEzLTRhMTAtYjZjZC0zODI3OGU3OGIyMWEiLCJ1bmlxdWVfbmFtZSI6Im1pcnVuYS5jaXVsdUBzdHVkZW50LnVwdC5ybyIsInVwbiI6Im1pcnVuYS5jaXVsdUBzdHVkZW50LnVwdC5ybyIsInV0aSI6IjFteWx3bGxXZUVPOUVmQnNjTE1uQUEiLCJ2ZXIiOiIxLjAifQ.SPa8nQa4VTirNhJEmvXqogghe3SqVRtvT_mUAc2GmfNPo5RQwbcZURC5R7QshPo3RolWfTZyjbQRicKD2iEE3TpjCVhPvGldWjiiyDlqT235CLykC6fQfilRAqb2voVO84cqaT8mH85j0fn7VfXODnXpsLlQpXM5NO8UiCMwK-wFlznUcXYujCrwOSCBIgW2oRE8oncMILHOD_OBlOEWWHbmfNByTWWCbHERl0c8hUPByJpDYtXk_I0SZ7XyVKxq6EE03NLPXIXtbx_jqo6GhwPuMW-yxzKQVXXIeDoAieq2a3vceGQTK1mR6PVkfzwZWhWsi5f35BVIVSZCBRp-5A";

var pills = {
  "Chlorthalidone 12.5 MG Oral Tablete": "1235144",
  "Bumetanide 0.25 MG": "1727569",
  "Captopril 100 MG Oral Tablet": "308962",
};

function createMedicationJSON(
  medicationName,
  patientId,
  patientName,
  dateOfPrescription,
  dosage
) {
  var medication = JSON.stringify({
    resourceType: "MedicationRequest",
    status: "active",
    intent: "plan",
    subject: {
      reference: "Patient/" + patientId,
      display: patientName,
    },
    authoredOn: dateOfPrescription,
    requester: {
      reference: "Practitioner/64154db1-714a-4488-b47c-a6575bc0f2b6",
    },
    medicationCodeableConcept: {
      coding: [
        {
          system: "http://www.nlm.nih.gov/research/umls/rxnorm",
          code: pills[medicationName],
          display: medicationName,
        },
      ],
      text: "",
    },
    dosageInstruction: [
      {
        sequence: 1,
        text: dosage,
      },
    ],
  });

  return medication;
}

//GET e din FHIR nostru
export function getServiceRequest(callBack) {
  callFHIRServer(
    "https://fgfhirclient.azurewebsites.net" + "/ServiceRequest",
    "GET",
    null,
    accessToken,
    callBack
  );
}

// noi trimitem in FHIR echipei celelalte
export function sendMedicationRequest(
  medicationName,
  patientId,
  patientName,
  dateOfPrescription,
  dosage,
  callBack
) {
  const medication = createMedicationJSON(
    medicationName,
    patientId,
    patientName,
    dateOfPrescription,
    dosage
  );
  callFHIRServer(
    "https://fgfhirclient.azurewebsites.net" + "/MedicationRequest",
    "POST",
    medication,
    accessToken,
    callBack
  );
}

function callFHIRServer(theUrl, method, message, accessToken, callBack) {
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200)
      callBack(JSON.parse(this.responseText));
  };
  xmlHttp.open(method, theUrl, true);
  xmlHttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  xmlHttp.setRequestHeader("Authorization", "Bearer " + accessToken);
  xmlHttp.send(message);
}

function FHIRGetCallback(res) {
  //id pacient (trebuie pus in sendMedicationRequest), nume pacient, nume medic de familie
  //   console.log(data);
  //   const parient = data.entry[0].resource.subject;
  //   sendMedicationRequest(
  //     "Chlorthalidone 12.5 MG Oral Tablete",
  //     parient.reference,
  //     parient.display,
  //     new Date().toISOString(),
  //     "O data pe zi."
  //   );

  //   let patientListHtml = "<ol>";
  //   data.entry.forEach(function (e) {
  //     patientListHtml +=
  //       "<li>" +
  //       e.resource.subject.reference +
  //       ", " +
  //       e.resource.subject.display +
  //       ", " +
  //       e.resource.requester.display;
  //   });
  //   return patientListHtml;
  console.log(res);
  return res;
}

function FHIRPostCallback(data) {
  console.log("bbbbbbbbbb");
  console.log(data);
}
