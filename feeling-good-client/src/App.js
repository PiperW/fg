import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";

//Redux
import { Provider } from "react-redux";
import store from "./redux/store";
import { isAuthenticated } from "./util/tokenDecoder";

//Theme
import { ThemeProvider as MuiThemeProvider } from "@material-ui/core/styles";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import themeSettings from "./util/theme";

//Pages
import Home from "./pages/home";
import LogIn from "./pages/login";
import SignUp from "./pages/signup";

//Components
import Navbar from "./components/Navbar";
import AuthRoute from "./util/AuthRoute";
import HomeAuthRoute from "./util/HomeAuthRoute";

const theme = createMuiTheme(themeSettings);

class App extends Component {
  constructor() {
    super();
    this.state = {
      authenticated: isAuthenticated(),
    };
    this.updateAuthStatus = this.updateAuthStatus.bind(this);
  }

  updateAuthStatus() {
    this.setState({ authenticated: isAuthenticated() });
  }

  render() {
    const { authenticated } = this.state;
    return (
      <div className="App">
        <MuiThemeProvider theme={theme}>
          <Router>
            <Navbar
              authenticated={authenticated}
              updateAuthStatus={this.updateAuthStatus}
            />
            <div className="container">
              <Switch>
                <HomeAuthRoute
                  exact
                  path="/"
                  component={Home}
                  authenticated={authenticated}
                  updateAuthStatus={this.updateAuthStatus}
                  redirectPath="/login"
                ></HomeAuthRoute>
                <AuthRoute
                  exact
                  path="/login"
                  component={LogIn}
                  authenticated={authenticated}
                  updateAuthStatus={this.updateAuthStatus}
                  redirectPath="/"
                ></AuthRoute>
                <AuthRoute
                  exact
                  path="/signup"
                  redirectPath="/"
                  component={SignUp}
                  authenticated={authenticated}
                  updateAuthStatus={this.updateAuthStatus}
                ></AuthRoute>
              </Switch>
            </div>
          </Router>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
