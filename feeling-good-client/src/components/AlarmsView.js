import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ErrorIcon from "@material-ui/icons/Error";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import InfoIcon from "@material-ui/icons/Info";
import Divider from "@material-ui/core/Divider";
import axios from "axios";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

const SEVERITY = {
  SUCCESS: "success",
  ERROR: "error",
  INFO: "info",
};

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    position: "relative",
    overflow: "auto",
    maxHeight: 525,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    padding: "15px",
  },
}));

function generateAlarms(
  alarms,
  setSelectedPatient,
  resolveSelectedAlarm,
  openSelectedPatientPopup
) {
  return alarms.map((alarm) => (
    <div key={alarm.id}>
      <ListItem>
        <ListItemAvatar>
          <ErrorIcon fontSize="large" />
        </ListItemAvatar>
        <ListItemText
          primary={alarm.title}
          secondary={
            new Date(alarm.createdAt) +
            " " +
            alarm.patientEmail +
            ": " +
            alarm.text
          }
        />
        <ListItemSecondaryAction
          onClick={() => {
            setSelectedPatient(alarm.patientEmail);
            openSelectedPatientPopup(alarm);
          }}
        >
          <IconButton edge="end" aria-label="delete">
            <InfoIcon />
          </IconButton>
          <IconButton edge="end" aria-label="delete">
            <CheckCircleIcon onClick={() => resolveSelectedAlarm(alarm)} />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
      <Divider variant="inset" component="li" />
    </div>
  ));
}

function useInterval(callback, delay) {
  const savedCallback = React.useRef();

  // Remember the latest callback.
  React.useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  React.useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

export default function AlarmsView(props) {
  const classes = useStyles();
  const [alarms, setAlarms] = React.useState([]);

  const HEADERS = {
    headers: {
      Authorization: localStorage.FBIdToken ? localStorage.FBIdToken : "",
      "Content-Type": "application/json",
    },
  };

  const [popup, setPopup] = React.useState({
    severity: SEVERITY.SUCCESS,
    text: "",
    open: false,
  });

  const handlePopupClose = () => {
    setPopup({
      severity: popup.severity,
      text: popup.text,
      open: false,
    });
  };

  const openSelectedPatientPopup = (rowData) =>
    setPopup({
      severity: SEVERITY.INFO,
      text: "You are now viewing " + rowData.patientEmail + "'s details.",
      open: true,
    });

  const fetchData = async () => {
    await axios
      .get("/alarms", HEADERS)
      .then((response) => {
        let data = [];
        response.data.forEach((alarm) => {
          data.push({
            id: alarm.id,
            doctorEmail: alarm.doctorEmail,
            patientEmail: alarm.patientEmail,
            text: alarm.text,
            createdAt: alarm.createdAt,
            status: alarm.status,
            title: alarm.title,
          });
        });
        setAlarms(data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  React.useEffect(() => {
    fetchData();
  }, []);

  /*useInterval(() => {
    fetchData();
  }, 60000);*/

  const resolveSelectedAlarm = (alarm) => {
    axios
      .post("/alarm/" + alarm.id, alarm, HEADERS)
      .then((res) => {
        const otherAlarms = alarms.filter((e) => e.id !== alarm.id);
        setAlarms(otherAlarms);
        setPopup({
          severity: SEVERITY.SUCCESS,
          text: res.data.message,
          open: true,
        });
      })
      .catch((error) => {
        setPopup({
          severity: SEVERITY.ERROR,
          text: error.response.data.error,
          open: true,
        });
      });
  };

  return (
    <div className={classes.root}>
      <Typography variant="h6" className={classes.title}>
        Alarms View
      </Typography>
      <div className={classes.demo}>
        <List>
          {generateAlarms(
            alarms,
            props.setSelectedPatient,
            resolveSelectedAlarm,
            openSelectedPatientPopup
          )}
        </List>
      </div>
      <Snackbar
        open={popup.open}
        autoHideDuration={6000}
        onClose={handlePopupClose}
      >
        <Alert onClose={handlePopupClose} severity={popup.severity}>
          {popup.text}
        </Alert>
      </Snackbar>
    </div>
  );
}
