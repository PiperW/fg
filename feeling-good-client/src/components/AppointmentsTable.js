import React from "react";
import MaterialTable from "material-table";
import axios from "axios";
import { getActiveUserEmail } from "../util/tokenDecoder";
import { correctMistakes } from "../util/dataValidators";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

const SEVERITY = {
  SUCCESS: "success",
  ERROR: "error",
};

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function AppointmentsTable(props) {
  const [entries, setEntries] = React.useState({
    data: [],
  });

  const HEADERS = {
    headers: {
      Authorization: localStorage.FBIdToken ? localStorage.FBIdToken : "",
      "Content-Type": "application/json",
    },
  };

  const [popup, setPopup] = React.useState({
    severity: SEVERITY.SUCCESS,
    text: "",
    open: false,
  });

  const [state] = React.useState({
    columns: [
      { title: "Scheduled for", field: "dateTime", type: "datetime" },
      { title: "Reason", field: "reason" },
      { title: "Simptoms", field: "simptoms" },
      { title: "Diagnostics", field: "diagnostics" },
      { title: "Treatment", field: "treatment" },
    ],
  });

  React.useEffect(() => {
    async function fetchData() {
      const { selectedPatient } = props;
      if (selectedPatient) {
        await axios
          .get("/appointments/" + selectedPatient, HEADERS)
          .then((response) => {
            let data = [];
            response.data.forEach((appointment) => {
              data.push({
                id: appointment.id,
                doctorEmail: appointment.doctorEmail,
                patientEmail: appointment.patientEmail,
                reason: appointment.reason,
                simptoms: appointment.simptoms,
                diagnostics: appointment.diagnostics,
                treatment: appointment.treatment,
                trimitereId: appointment.trimitereId,
                dateTime: appointment.dateTime,
              });
            });
            console.log(response);
            setEntries({ data: data });
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    }
    fetchData();
  }, [props.selectedPatient]);

  const handlePopupClose = () => {
    setPopup({
      severity: popup.severity,
      text: popup.text,
      open: false,
    });
  };

  return (
    <div>
      <MaterialTable
        title="Appointments View"
        columns={state.columns}
        data={entries.data}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                const data = [...entries.data];
                newData.patientEmail = props.selectedPatient;
                newData.doctorEmail = getActiveUserEmail();
                axios
                  .post("/appointment", newData, HEADERS)
                  .then((res) => {
                    newData.id = res.data.id;
                    data.push(newData);
                    setEntries({ ...entries, data });
                    setPopup({
                      severity: SEVERITY.SUCCESS,
                      text: "Appointment added successfully.",
                      open: true,
                    });
                  })
                  .catch((error) => {
                    console.log(error);
                    setPopup({
                      severity: SEVERITY.ERROR,
                      text: error.response.data.error,
                      open: true,
                    });
                  });
              }, 600);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                let data = [...entries.data];
                data[data.map((e) => e.id).indexOf(oldData.id)] = newData;
                axios
                  .post("/appointment/" + newData.id, newData, HEADERS)
                  .then((res) => {
                    setEntries({ ...entries, data });
                    setPopup({
                      severity: SEVERITY.SUCCESS,
                      text: res.data.message,
                      open: true,
                    });
                  })
                  .catch((error) => {
                    setPopup({
                      severity: SEVERITY.ERROR,
                      text: error.response.data.error,
                      open: true,
                    });
                  });
              }, 600);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                const data = [...entries.data];
                data.splice(data.map((e) => e.id).indexOf(oldData.id), 1);
                axios
                  .delete("/appointment/" + oldData.id, HEADERS)
                  .then((res) =>
                    setPopup({
                      severity: SEVERITY.SUCCESS,
                      text: res.data.message,
                      open: true,
                    })
                  )
                  .catch((error) =>
                    setPopup({
                      severity: SEVERITY.ERROR,
                      text: error.response.data.error,
                      open: true,
                    })
                  );
                setEntries({ ...entries, data });
              }, 600);
            }),
        }}
      />

      <Snackbar
        open={popup.open}
        autoHideDuration={6000}
        onClose={handlePopupClose}
      >
        <Alert onClose={handlePopupClose} severity={popup.severity}>
          {popup.text}
        </Alert>
      </Snackbar>
    </div>
  );
}
