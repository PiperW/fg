import React from "react";
import MaterialTable from "material-table";
import axios from "axios";
import { getActiveUserEmail } from "../util/tokenDecoder";
import { correctMistakes } from "../util/dataValidators";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

const SEVERITY = {
  SUCCESS: "success",
  ERROR: "error",
  INFO: "info",
};

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function PatientsTable(props) {
  const [entries, setEntries] = React.useState({
    data: [],
  });

  const HEADERS = {
    headers: {
      Authorization: localStorage.FBIdToken ? localStorage.FBIdToken : "",
      "Content-Type": "application/json",
    },
  };

  const [popup, setPopup] = React.useState({
    severity: SEVERITY.SUCCESS,
    text: "",
    open: false,
  });

  const [selectedRow, setSelectedRow] = React.useState(null);

  const handlePopupClose = () => {
    setPopup({
      severity: popup.severity,
      text: popup.text,
      open: false,
    });
  };

  const [state] = React.useState({
    columns: [
      { title: "ID", field: "cnp" },
      { title: "Full name", field: "fullName" },
      { title: "Date of birth", field: "birthday", type: "date" },
      { title: "Address", field: "address" },
      { title: "E-mail", field: "email" },
      { title: "Blood type", field: "bloodType" },
      { title: "Allergies", field: "allergies" },
      { title: "Affections", field: "affections" },
    ],
  });

  React.useEffect(() => {
    async function fetchData() {
      const response = await axios
        .get("/patients/" + getActiveUserEmail(), HEADERS)
        .then((response) => {
          let data = [];
          response.data.forEach((patient) => {
            data.push({
              email: patient.email,
              fullName: patient.fullName,
              birthday: patient.birthday,
              address: patient.address,
              bloodType: patient.bloodType,
              allergies: patient.allergies,
              affections: patient.affections,
              cnp: patient.cnp,
            });
          });
          console.log(response);
          setEntries({ data: data });
        })
        .catch(function (error) {
          console.log(error);
        });
    }
    fetchData();
  }, []);

  return (
    <div>
      <MaterialTable
        title="Patients View"
        columns={state.columns}
        data={entries.data}
        options={{
          rowStyle: (rowData) => ({
            backgroundColor:
              selectedRow === rowData.tableData.id ? "#f5f5f5" : "#FFF",
          }),
        }}
        actions={[
          {
            icon: "info",
            tooltip: "Select User",
            onClick: (event, rowData) => {
              props.setSelectedPatient(rowData.email);
              setSelectedRow(rowData.tableData.id);
              setPopup({
                severity: SEVERITY.INFO,
                text: "You are now viewing " + rowData.fullName + "'s details.",
                open: true,
              });
            },
          },
        ]}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                const data = [...entries.data];
                data.push(newData);
                axios
                  .post("/patient", newData, HEADERS)
                  .then((res) => {
                    setEntries({ ...entries, data });
                    setPopup({
                      severity: SEVERITY.SUCCESS,
                      text: res.data.message,
                      open: true,
                    });
                  })
                  .catch((error) => {
                    console.log(error);
                    setPopup({
                      severity: SEVERITY.ERROR,
                      text: error.response.data.error,
                      open: true,
                    });
                  });
              }, 600);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                let data = [...entries.data];
                data[data.map((e) => e.email).indexOf(oldData.email)] = newData;
                axios
                  .post("/patient", newData, HEADERS)
                  .then((res) => {
                    setEntries({ ...entries, data });
                    setPopup({
                      severity: SEVERITY.SUCCESS,
                      text: res.data.message,
                      open: true,
                    });
                  })
                  .catch((error) => {
                    setPopup({
                      severity: SEVERITY.ERROR,
                      text: error.response.data.error,
                      open: true,
                    });
                  });
              }, 600);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                const data = [...entries.data];
                data.splice(data.map((e) => e.email).indexOf(oldData.email), 1);
                axios
                  .delete("/patient/" + oldData.email, HEADERS)
                  .then((res) =>
                    setPopup({
                      severity: SEVERITY.SUCCESS,
                      text: res.data.message,
                      open: true,
                    })
                  )
                  .catch((error) =>
                    setPopup({
                      severity: SEVERITY.ERROR,
                      text: error.response.data.error,
                      open: true,
                    })
                  );
                setEntries({ ...entries, data });
              }, 600);
            }),
        }}
      />

      <Snackbar
        open={popup.open}
        autoHideDuration={6000}
        onClose={handlePopupClose}
      >
        <Alert onClose={handlePopupClose} severity={popup.severity}>
          {popup.text}
        </Alert>
      </Snackbar>
    </div>
  );
}
