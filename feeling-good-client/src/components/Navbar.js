import React, { Component } from "react";
import { Link } from "react-router-dom";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";

class Navbar extends Component {
  constructor() {
    super();
    this.logOut = this.logOut.bind(this);
  }
  logOut() {
    localStorage.removeItem("FBIdToken");
    this.props.updateAuthStatus();
  }
  render() {
    return (
      <AppBar>
        <Toolbar className="nav-container">
          {this.props.authenticated ? (
            <div>
              <Button
                color="inherit"
                component={Link}
                to="/"
                onClick={this.props.updateAuthStatus}
              >
                Home
              </Button>
              <Button
                color="inherit"
                component={Link}
                to="/login"
                onClick={this.logOut}
              >
                Log out
              </Button>
            </div>
          ) : (
            <div>
              <Button
                color="inherit"
                component={Link}
                to="/login"
                onClick={this.props.updateAuthStatus}
              >
                Log in
              </Button>
              <Button
                color="inherit"
                component={Link}
                to="/signup"
                onClick={this.props.updateAuthStatus}
              >
                Sign up
              </Button>
            </div>
          )}
        </Toolbar>
      </AppBar>
    );
  }
}

export default Navbar;
