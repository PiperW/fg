import React from "react";
import MaterialTable from "material-table";
import axios from "axios";
import { getActiveUserEmail } from "../util/tokenDecoder";
import { correctMistakes } from "../util/dataValidators";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

const SEVERITY = {
  SUCCESS: "success",
  ERROR: "error",
};

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function NotificationsView(props) {
  const [entries, setEntries] = React.useState({
    data: [],
  });

  const HEADERS = {
    headers: {
      Authorization: localStorage.FBIdToken ? localStorage.FBIdToken : "",
      "Content-Type": "application/json",
    },
  };

  const [popup, setPopup] = React.useState({
    severity: SEVERITY.SUCCESS,
    text: "",
    open: false,
  });

  const [state] = React.useState({
    columns: [
      {
        title: "Created at",
        field: "dateTime",
        type: "datetime",
        editable: "never",
      },
      { title: "Text", field: "text" },
    ],
  });

  React.useEffect(() => {
    async function fetchData() {
      const { selectedPatient } = props;
      if (selectedPatient) {
        await axios
          .get("/notifications/" + selectedPatient, HEADERS)
          .then((response) => {
            let data = [];
            response.data.forEach((appointment) => {
              data.push({
                id: appointment.id,
                doctorEmail: appointment.doctorEmail,
                patientEmail: appointment.patientEmail,
                text: appointment.text,
                dateTime: appointment.dateTime,
              });
            });
            console.log(response);
            setEntries({ data: data });
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    }
    fetchData();
  }, [props.selectedPatient]);

  const handlePopupClose = () => {
    setPopup({
      severity: popup.severity,
      text: popup.text,
      open: false,
    });
  };

  return (
    <div>
      <MaterialTable
        title="Notifications View"
        columns={state.columns}
        data={entries.data}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                const data = [...entries.data];
                newData.patientEmail = props.selectedPatient;
                newData.doctorEmail = getActiveUserEmail();
                axios
                  .post("/notification", newData, HEADERS)
                  .then((res) => {
                    newData.id = res.data.id;
                    newData.dateTime = res.data.date_time;
                    data.push(newData);
                    setEntries({ ...entries, data });
                    setPopup({
                      severity: SEVERITY.SUCCESS,
                      text: "Notification added successfully.",
                      open: true,
                    });
                  })
                  .catch((error) => {
                    console.log(error);
                    setPopup({
                      severity: SEVERITY.ERROR,
                      text: error.response.data.error,
                      open: true,
                    });
                  });
              }, 600);
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                let data = [...entries.data];
                data[data.map((e) => e.id).indexOf(oldData.id)] = newData;
                axios
                  .post("/notification/" + newData.id, newData, HEADERS)
                  .then((res) => {
                    setEntries({ ...entries, data });
                    setPopup({
                      severity: SEVERITY.SUCCESS,
                      text: res.data.message,
                      open: true,
                    });
                  })
                  .catch((error) => {
                    setPopup({
                      severity: SEVERITY.ERROR,
                      text: error.response.data.error,
                      open: true,
                    });
                  });
              }, 600);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              setTimeout(() => {
                resolve();
                const data = [...entries.data];
                data.splice(data.map((e) => e.id).indexOf(oldData.id), 1);
                axios
                  .delete("/notification/" + oldData.id, HEADERS)
                  .then((res) =>
                    setPopup({
                      severity: SEVERITY.SUCCESS,
                      text: res.data.message,
                      open: true,
                    })
                  )
                  .catch((error) =>
                    setPopup({
                      severity: SEVERITY.ERROR,
                      text: error.response.data.error,
                      open: true,
                    })
                  );
                setEntries({ ...entries, data });
              }, 600);
            }),
        }}
      />

      <Snackbar
        open={popup.open}
        autoHideDuration={6000}
        onClose={handlePopupClose}
      >
        <Alert onClose={handlePopupClose} severity={popup.severity}>
          {popup.text}
        </Alert>
      </Snackbar>
    </div>
  );
}
