import React from "react";
import { TimeSeries } from "pondjs";

import {
  ChartContainer,
  ChartRow,
  Charts,
  YAxis,
  LineChart,
  Baseline,
  Resizable,
} from "react-timeseries-charts";

const style = {
  value: {
    stroke: "#fa439f",
    opacity: 0.2,
    strokeWidth: 2,
  },
};

const baselineStyle = {
  line: {
    stroke: "orange",
    strokeWidth: 2,
    opacity: 0.4,
    strokeDasharray: "none",
  },
  label: {
    fill: "orange",
  },
};

const baselineStyleLite = {
  line: {
    stroke: "orange",
    strokeWidth: 2,
    opacity: 0.5,
  },
  label: {
    fill: "orange",
  },
};

const baselineStyleExtraLite = {
  line: {
    stroke: "orange",
    strokeWidth: 2,
    opacity: 0.2,
    strokeDasharray: "1,1",
  },
  label: {
    fill: "orange",
  },
};

export class LineChartTry extends React.Component {
  constructor(props) {
    super(props);
  }

  handleTrackerChanged = (tracker) => {
    this.setState({ tracker });
  };

  handleTimeRangeChange = (timerange) => {
    this.setState({ timerange });
  };

  render() {
    const series = new TimeSeries(this.props.data);
    return (
      <Resizable>
        <ChartContainer
          title={this.props.title}
          titleStyle={{ fill: "#555", fontWeight: 500 }}
          timeRange={series.range()}
          format="%b %d, %H:%M"
          timeAxisTickCount={10}
        >
          <ChartRow height="400">
            <YAxis
              id={this.props.axisId}
              label={
                this.props.title +
                (this.props.unit ? " [" + this.props.unit + "]" : "")
              }
              min={series.min()}
              max={series.max()}
              width="50"
            />
            <Charts>
              <LineChart
                axis={this.props.axisId}
                series={series}
                style={style}
              />
              <Baseline
                axis={this.props.axisId}
                style={baselineStyleLite}
                value={this.props.maxVal}
                label="Max"
                position="right"
              />
              <Baseline
                axis={this.props.axisId}
                style={baselineStyleLite}
                value={this.props.minVal}
                label="Min"
                position="right"
              />
              <Baseline
                axis={this.props.axisId}
                style={baselineStyleExtraLite}
                value={series.avg() - series.stdev()}
              />
              <Baseline
                axis={this.props.axisId}
                style={baselineStyleExtraLite}
                value={series.avg() + series.stdev()}
              />
              <Baseline
                axis={this.props.axisId}
                style={baselineStyle}
                value={series.avg()}
                label="Avg"
                position="right"
              />
            </Charts>
          </ChartRow>
        </ChartContainer>
      </Resizable>
    );
  }
}
