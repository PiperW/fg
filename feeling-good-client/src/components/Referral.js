import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import InfoIcon from "@material-ui/icons/Info";
import Divider from "@material-ui/core/Divider";
import axios from "axios";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { sendMedicationRequest } from "../util/fhir";
import LocalHospitalIcon from "@material-ui/icons/LocalHospital";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import SendIcon from "@material-ui/icons/Send";

const useStyles = makeStyles((theme) => ({
  input: {
    marginLeft: "30px",
  },
}));

export function Referral(props) {
  const classes = useStyles();
  const [pill, setPill] = React.useState("Chlorthalidone 12.5 MG Oral Tablete");
  const [dosage, setDosage] = React.useState("");

  const handlePillChange = (evt) => {
    setPill(evt.target.value);
  };

  const handleDosageChange = (evt) => {
    setDosage(evt.target.value);
  };

  const pills = [
    {
      value: "Chlorthalidone 12.5 MG Oral Tablete",
      label: "Chlorthalidone 12.5 MG Oral Tablete",
    },
    {
      value: "Bumetanide 0.25 MG",
      label: "Bumetanide 0.25 MG",
    },
    {
      value: "Captopril 100 MG Oral Tablet",
      label: "Captopril 100 MG Oral Tablet",
    },
  ];
  const sendMedication = () => {
    const f = async () => {
      await sendMedicationRequest(
        pill,
        props.patient.reference,
        props.patient.display,
        new Date().toISOString(),
        dosage,
        (res) => console.log("Mergeeee " + res)
      );
    };
    f();
  };

  return (
    <div key={props.patient.reference}>
      <ListItem>
        <ListItemAvatar>
          <LocalHospitalIcon fontSize="large" />
        </ListItemAvatar>
        <ListItemText
          primary={"Referral from Dr. " + props.doctor.display}
          secondary={"Patient: " + props.patient.display}
        />
        <TextField
          className={classes.input}
          id="Medication"
          select
          label="Medication"
          value={pill}
          onChange={handlePillChange}
          helperText="Please select the recommended medication"
        >
          {pills.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
        <TextField
          className={classes.input}
          id="Dosage"
          label="Dosage"
          onChange={handleDosageChange}
          helperText="Please write some instructions"
        ></TextField>
        <ListItemSecondaryAction>
          <IconButton edge="end" aria-label="delete" onClick={sendMedication}>
            <SendIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
      <Divider variant="inset" component="li" />
    </div>
  );
}
