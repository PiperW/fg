import React from "react";
import PropTypes from "prop-types";
import SwipeableViews from "react-swipeable-views";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import { LineChartTry } from "./LineChart";
import axios from "axios";
import ValueSetter from "./ValueSetter";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      style={{ height: 600 }}
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3} style={{ height: 600 }}>
          <Typography
            component={"span"}
            variant={"body2"}
            style={{ height: 600 }}
          >
            {" "}
            {children}
          </Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    "aria-controls": `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
  graph: {
    height: "1000px",
  },
});

function useInterval(callback, delay) {
  const savedCallback = React.useRef();

  // Remember the latest callback.
  React.useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  React.useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

const SEVERITY = {
  SUCCESS: "success",
  ERROR: "error",
  INFO: "info",
};

export default function StatisticsView(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const HEADERS = {
    headers: {
      Authorization: localStorage.FBIdToken ? localStorage.FBIdToken : "",
      "Content-Type": "application/json",
    },
  };

  const [popup, setPopup] = React.useState({
    severity: SEVERITY.SUCCESS,
    text: "",
    open: false,
  });
  const [loading, setLoading] = React.useState(false);
  const [normalValues, setNormalValues] = React.useState({
    id: null,
    maxEcg: 0,
    maxTemperature: 0,
    maxHumidity: 0,
    minEcg: 0,
    minTemperature: 0,
    minHumidity: 0,
    email: props.selectedPatient,
  });
  const [chartData, setChartData] = React.useState({
    ecg: {
      name: "ECG",
      columns: ["time", "value"],
      points: [[0, 0]],
    },
    pulse: {
      name: "Pulse",
      columns: ["time", "value"],
      points: [[0, 0]],
    },
    temperature: {
      name: "Temperature",
      columns: ["time", "value"],
      points: [[0, 0]],
    },
    humidity: {
      name: "Humidity",
      columns: ["time", "value"],
      points: [[0, 0]],
    },
  });

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const handlePopupClose = () => {
    setPopup({
      severity: popup.severity,
      text: popup.text,
      open: false,
    });
  };

  const fetchNormalValues = async () => {
    const response = await axios
      .get("/stats/normalValues/" + props.selectedPatient, HEADERS)
      .then((response) => {
        const formattedData = {
          id: response.data.id,
          maxEcg: parseInt(response.data.maxEcg),
          maxTemperature: parseFloat(response.data.maxTemperature),
          maxHumidity: parseFloat(response.data.maxHumidity),
          minEcg: parseInt(response.data.minEcg),
          minTemperature: parseFloat(response.data.minTemperature),
          minHumidity: parseFloat(response.data.minHumidity),
          email: response.data.email,
        };
        console.log(formattedData);
        setNormalValues(formattedData);
      })
      .catch(function (error) {
        setNormalValues({
          id: null,
          maxEcg: 0,
          maxTemperature: 0,
          maxHumidity: 0,
          minEcg: 0,
          minTemperature: 0,
          minHumidity: 0,
          email: props.selectedPatient,
        });
        console.log(error);
      });
  };

  const fetchChartData = async () => {
    const response = await axios
      .get("/stats/" + props.selectedPatient, HEADERS)
      .then((response) => {
        setChartData(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const setEcgValue = (obj) => {
    console.log(obj);
    let newValues = normalValues;
    if (obj.maxValue) {
      newValues.maxEcg = parseInt(obj.maxValue);
    }
    if (obj.minValue) {
      newValues.minEcg = parseInt(obj.minValue);
    }
    console.log(newValues);
    setNormalValues(newValues);
  };

  const setTemperatureValue = (obj) => {
    console.log(obj);
    let newValues = normalValues;
    if (obj.maxValue) {
      newValues.maxTemperature = parseInt(obj.maxValue);
    }
    if (obj.minValue) {
      newValues.minTemperature = parseInt(obj.minValue);
    }
    console.log(newValues);
    setNormalValues(newValues);
  };

  const setHumidityValue = (obj) => {
    console.log(obj);
    let newValues = normalValues;
    if (obj.maxValue) {
      newValues.maxHumidity = parseInt(obj.maxValue);
    }
    if (obj.minValue) {
      newValues.minHumidity = parseInt(obj.minValue);
    }
    console.log(newValues);
    setNormalValues(newValues);
  };

  const saveValues = async () => {
    setLoading(true);
    await axios
      .post("/stats/normalValues", normalValues, HEADERS)
      .then((res) => {
        if (res.data) {
          if (res.data.id) {
            normalValues.id = res.data.id;
            setNormalValues(normalValues);
          }
          setPopup({
            severity: SEVERITY.INFO,
            text: "Range updated successfully.",
            open: true,
          });
          setLoading(false);
        }
      })
      .catch(function (error) {
        setPopup({
          severity: SEVERITY.ERROR,
          text: error.response.data.error,
          open: true,
        });
        setLoading(false);
      });
  };

  React.useEffect(() => {
    fetchChartData();
  }, [props.selectedPatient]);

  React.useEffect(() => {
    fetchNormalValues();
  }, [props.selectedPatient]);

  /*useInterval(() => {
    fetchChartData();
  }, 60000);*/

  return (
    <div className={classes.root}>
      <Paper className={classes.root}>
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="ECG" {...a11yProps(0)} />
          <Tab label="Temperature" {...a11yProps(1)} />
          <Tab label="Humidity" {...a11yProps(2)} />
        </Tabs>
      </Paper>
      <SwipeableViews
        axis={theme.direction === "rtl" ? "x-reverse" : "x"}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <ValueSetter
            minValue={normalValues.minEcg}
            maxValue={normalValues.maxEcg}
            setNormalValue={setEcgValue}
            saveNormalValues={saveValues}
            loading={loading}
          ></ValueSetter>
          <LineChartTry
            data={chartData.ecg}
            axisId="ecg"
            title="ECG"
            minVal={normalValues.minEcg}
            maxVal={normalValues.maxEcg}
          ></LineChartTry>
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          <ValueSetter
            minValue={normalValues.minTemperature}
            maxValue={normalValues.maxTemperature}
            setNormalValue={setTemperatureValue}
            saveNormalValues={saveValues}
            loading={loading}
          ></ValueSetter>
          <LineChartTry
            data={chartData.temperature}
            axisId="temperature"
            title="Temperature"
            unit="°C"
            minVal={normalValues.minTemperature}
            maxVal={normalValues.maxTemperature}
          ></LineChartTry>
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
          <ValueSetter
            minValue={normalValues.minHumidity}
            maxValue={normalValues.maxHumidity}
            setNormalValue={setHumidityValue}
            saveNormalValues={saveValues}
            loading={loading}
          ></ValueSetter>
          <LineChartTry
            data={chartData.humidity}
            axisId="humidity"
            title="Humidity"
            minVal={normalValues.minHumidity}
            maxVal={normalValues.maxHumidity}
            unit="%"
          ></LineChartTry>
        </TabPanel>
      </SwipeableViews>
      <Snackbar
        open={popup.open}
        autoHideDuration={6000}
        onClose={handlePopupClose}
      >
        <Alert onClose={handlePopupClose} severity={popup.severity}>
          {popup.text}
        </Alert>
      </Snackbar>
    </div>
  );
}
