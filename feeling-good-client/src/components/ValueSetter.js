import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import AppIcon from "../images/feeling-good.png";
import axios from "axios";

//MUI stuff
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";
import IconButton from "@material-ui/core/IconButton";
import SaveIcon from "@material-ui/icons/Save";
import { getServiceRequest, sendMedicationRequest } from "../util/fhir";

const styles = {
  input: {
    margin: "3px",
  },
};
class ValueSetter extends Component {
  constructor(props) {
    super(props);
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.saveNormalValues();
  };

  handleChange = (event) => {
    this.props.setNormalValue({
      [event.target.name]: event.target.value,
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Typography className={classes.input} variant="h5">
          Normal range
        </Typography>
        <Typography className={classes.input} variant="body2" gutterBottom>
          {"Min Value: " +
            this.props.minValue +
            ", Max Value: " +
            this.props.maxValue}
        </Typography>
        <form noValidate>
          <TextField
            id="min"
            label="Min Value"
            type="number"
            InputLabelProps={{
              shrink: true,
            }}
            name="minValue"
            onChange={this.handleChange}
            className={classes.input}
          />
          <TextField
            id="maxValue"
            label="Max Value"
            type="number"
            InputLabelProps={{
              shrink: true,
            }}
            name="maxValue"
            onChange={this.handleChange}
            className={classes.input}
          />
          <IconButton aria-label="delete" onClick={this.handleSubmit}>
            <SaveIcon />
          </IconButton>

          {this.props.loading && <CircularProgress size={30} />}
        </form>
      </div>
    );
  }
}

ValueSetter.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ValueSetter);
