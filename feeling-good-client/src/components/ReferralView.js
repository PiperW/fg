import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import InfoIcon from "@material-ui/icons/Info";
import Divider from "@material-ui/core/Divider";
import axios from "axios";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { getServiceRequest, sendMedicationRequest } from "../util/fhir";
import LocalHospitalIcon from "@material-ui/icons/LocalHospital";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import { Referral } from "./Referral";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    position: "relative",
    overflow: "auto",
    maxHeight: 525,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    padding: "15px",
  },
}));

function useInterval(callback, delay) {
  const savedCallback = React.useRef();

  // Remember the latest callback.
  React.useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  // Set up the interval.
  React.useEffect(() => {
    function tick() {
      savedCallback.current();
    }
    if (delay !== null) {
      let id = setInterval(tick, delay);
      return () => clearInterval(id);
    }
  }, [delay]);
}

export default function ReferralView(props) {
  const classes = useStyles();
  const [referrals, setReferrals] = React.useState([]);

  const generateReferrals = () => {
    return referrals.map((referral) => {
      const patient = referral.resource.subject;
      const doctor = referral.resource.requester;
      return <Referral patient={patient} doctor={doctor}></Referral>;
    });
  };

  const fetchData = async () => {
    await getServiceRequest((res) => setReferrals(res.entry));
  };

  React.useEffect(() => {
    fetchData();
  }, []);

  useInterval(() => {
    fetchData();
  }, 10000);

  return (
    <div className={classes.root}>
      <Typography variant="h6" className={classes.title}>
        Referrals View
      </Typography>
      <div className={classes.demo}>
        <List>{generateReferrals()}</List>
      </div>
    </div>
  );
}
