import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import PatientsTable from "../components/PatientsTable";
import AppointmentsTable from "../components/AppointmentsTable";
import AlarmsView from "../components/AlarmsView";
import NotificationsView from "../components/NotificationsView";
import StatisticsView from "../components/StatisticsView";
import ReferralView from "../components/ReferralView";

const styles = {
  root: {
    flexGrow: 1,
  },
  paper: {},
};

class Home extends Component {
  constructor() {
    super();
    this.state = {
      selectedPatient: null,
    };
    this.setSelectedPatient = this.setSelectedPatient.bind(this);
  }
  setSelectedPatient(email) {
    this.setState({
      selectedPatient: email,
    });
  }
  render() {
    const { classes } = this.props;
    const { selectedPatient } = this.state;
    return (
      <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={12} lg={8}>
            <Paper className={classes.paper}>
              <PatientsTable
                setSelectedPatient={this.setSelectedPatient}
              ></PatientsTable>
            </Paper>
          </Grid>
          <Grid item xs={12} lg={4}>
            <Paper className={classes.paper}>
              <AlarmsView
                setSelectedPatient={this.setSelectedPatient}
              ></AlarmsView>
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <StatisticsView
                selectedPatient={selectedPatient}
              ></StatisticsView>
            </Paper>
          </Grid>
          <Grid item xs={12} lg={6}>
            <Paper className={classes.paper}>
              <AppointmentsTable
                selectedPatient={selectedPatient}
              ></AppointmentsTable>
            </Paper>
          </Grid>
          <Grid item xs={12} lg={6}>
            <Paper className={classes.paper}>
              <NotificationsView
                selectedPatient={selectedPatient}
              ></NotificationsView>
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <ReferralView></ReferralView>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

Home.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Home);
