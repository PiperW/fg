const functions = require("firebase-functions");

const app = require("express")();
const { dataBase } = require("./util/admin");

const {
  getAllScreams,
  postOneScream,
  getScream,
  commentOnScream,
  likeScream,
  unlikeScream,
  deleteScream,
} = require("./handlers/screams");
const {
  signUp,
  logIn,
  uploadImage,
  getAuthenticatedUser,
  getUserDetails,
  markNotificationsRead,
} = require("./handlers/auth");

const {
  getPatientsForDoctor,
  addPatientDetails,
  deletePatient,
} = require("./handlers/tableOfPatients");

const {
  getAppointmentsForPatient,
  addAppointment,
  updateAppointment,
  deleteAppointment,
} = require("./handlers/appointmentsTable");

const {
  getNotificationsForPatient,
  addNotification,
  updateNotification,
  deleteNotification,
} = require("./handlers/notificationsTable");

const { getAlarms, resolveAlarm } = require("./handlers/alarmsView");

const {
  getStats,
  getNormalValues,
  saveNormalValues,
} = require("./handlers/statisticsView");
const FBAuth = require("./util/firebaseAuth");

//Scream routes
app.get("/screams", getAllScreams);
app.post("/scream", FBAuth, postOneScream);
app.get("/scream/:screamId", getScream);
app.post("/scream/:screamId/comment", FBAuth, commentOnScream);
app.get("/scream/:screamId/like", FBAuth, likeScream);
app.get("/scream/:screamId/unlike", FBAuth, unlikeScream);

//User routes

app.post("/user/image", FBAuth, uploadImage);
app.get("/user", FBAuth, getAuthenticatedUser);
app.get("/user/:handle", getUserDetails);
app.post("/notifications", FBAuth, markNotificationsRead);

app.post("/signup", signUp);
app.post("/login", logIn);

app.get("/patients/:doctorEmail", FBAuth, getPatientsForDoctor);
app.delete("/patient/:patientEmail", FBAuth, deletePatient);
app.post("/patient", FBAuth, addPatientDetails);

app.get("/appointments/:patientEmail", FBAuth, getAppointmentsForPatient);
app.post("/appointment", FBAuth, addAppointment);
app.post("/appointment/:appointmentId", FBAuth, updateAppointment);
app.delete("/appointment/:appointmentId", FBAuth, deleteAppointment);

app.get("/notifications/:patientEmail", FBAuth, getNotificationsForPatient);
app.post("/notification", FBAuth, addNotification);
app.post("/notification/:notificationId", FBAuth, updateNotification);
app.delete("/notification/:notificationId", FBAuth, deleteNotification);

app.get("/alarms", FBAuth, getAlarms);
app.post("/alarm/:alarmId", FBAuth, resolveAlarm);

app.get("/stats/:patientEmail", FBAuth, getStats);
app.get("/stats/normalValues/:patientEmail", FBAuth, getNormalValues);
app.post("/stats/normalValues", FBAuth, saveNormalValues);

exports.api = functions.region("europe-west2").https.onRequest(app);

exports.createNotificationOnLike = functions
  .region("europe-west2")
  .firestore.document("likes/{id}")
  .onCreate((snapshot) => {
    return dataBase
      .doc(`/screams/${snapshot.data().screamId}`)
      .get()
      .then((doc) => {
        if (doc.exists && doc.data().userEmail !== snapshot.data().userEmail) {
          return dataBase.doc(`/notifications/${snapshot.id}`).set({
            createdAt: new Date().toISOString(),
            recipient: doc.data().userEmail,
            sender: snapshot.data().userEmail,
            type: "like",
            read: false,
            screamId: doc.id,
          });
        }
      })
      .catch((error) => console.error(error));
  });

exports.deleteNotificationOnUnlike = functions
  .region("europe-west2")
  .firestore.document("likes/{id}")
  .onDelete((snapshot) => {
    return dataBase
      .doc(`/notifications/${snapshot.id}`)
      .delete()
      .catch((error) => {
        console.error(error);
        return;
      });
  });

exports.createNotificationOnComment = functions
  .region("europe-west2")
  .firestore.document("comments/{id}")
  .onCreate((snapshot) => {
    return dataBase
      .doc(`/screams/${snapshot.data().screamId}`)
      .get()
      .then((doc) => {
        if (doc.exists && doc.data().userEmail !== snapshot.data().userEmail) {
          return dataBase.doc(`/notifications/${snapshot.id}`).set({
            createdAt: new Date().toISOString(),
            recipient: doc.data().userEmail,
            sender: snapshot.data().userEmail,
            type: "comment",
            read: false,
            screamId: doc.id,
          });
        }
      })
      .catch((error) => {
        console.error(error);
        return;
      });
  });

exports.onUserImageChange = functions
  .region("europe-west2")
  .firestore.document("/user/{userId}")
  .onUpdate((change) => {
    console.log(change.before.data());
    console.log(change.after.data());
    if (change.before.data().imageUrl !== change.after.data().imageUrl) {
      console.log("image has changed");
      let batch = dataBase.batch();
      console.log(batch);
      console.log(change.before.data().email);
      return dataBase
        .collection("screams")
        .where("userEmail", "==", change.before.data().email)
        .get()
        .then((data) => {
          console.log(data);
          data.forEach((doc) => {
            const scream = dataBase.doc(`/screams/${doc.id}`);
            console.log(scream);
            batch.update(scream, { userImage: change.after.data().imageUrl });
          });
          return batch.commit();
        });
    } else return true;
  });

exports.onScreamDelete = functions
  .region("europe-west2")
  .firestore.document("/screams/{screamId}")
  .onDelete((snapshot, context) => {
    const screamId = context.params.screamId;
    const batch = dataBase.batch();
    return dataBase
      .collection("comments")
      .where("screamId", "==", screamId)
      .get()
      .then((data) => {
        data.forEach((doc) => {
          batch.delete(dataBase.doc(`/comments/${doc.id}`));
        });
        return dataBase
          .collection("likes")
          .where("screamId", "==", screamId)
          .get();
      })
      .then((data) => {
        data.forEach((doc) => {
          batch.delete(dataBase.doc(`/likes/${doc.id}`));
        });
        return dataBase
          .collection("notifications")
          .where("screamId", "==", screamId)
          .get();
      })
      .then((data) => {
        data.forEach((doc) => {
          batch.delete(dataBase.doc(`/notifications/${doc.id}`));
        });
        return batch.commit();
      })
      .catch((error) => console.error(error));
  });
