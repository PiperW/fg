const { dataBase } = require("../util/admin");

exports.getNotificationsForPatient = (request, response) => {
  dataBase
    .doc(`/patient/${request.params.patientEmail}`)
    .get()
    .then((doc) => {
      if (doc.exists) {
        return dataBase
          .collection("notifications")
          .where("patient_email", "==", request.params.patientEmail)
          .get();
      } else {
        return response.status(404).json({ error: "Patient not found." });
      }
    })
    .then((data) => {
      const notifications = [];
      data.forEach((doc) => {
        notifications.push({
          id: doc.id,
          doctorEmail: doc.data().doctor_email,
          patientEmail: doc.data().patient_email,
          text: doc.data().text,
          dateTime: doc.data().date_time,
        });
      });
      return response.json(notifications);
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};

exports.addNotification = (request, response) => {
  const newNotification = {
    doctor_email: request.user.email ? request.user.email : "",
    patient_email: request.body.patientEmail ? request.body.patientEmail : "",
    text: request.body.text ? request.body.text : "",
    date_time: new Date().toISOString(),
  };
  console.log(request.body);
  dataBase
    .doc(`/patient/${request.body.patientEmail}`)
    .get()
    .then((doc) => {
      if (doc.exists && doc.data().doctor_email === request.user.email) {
        console.log(request.body);
        return dataBase.collection("notifications").add(newNotification);
      } else if (doc.exists && doc.data().doctor_email !== request.user.email) {
        return response.status(403).json({
          error: "Unauthorized to send notifications to this patient.",
        });
      } else {
        return response.status(404).json({ error: "Patient not found." });
      }
    })
    .then((element) => {
      const resNotification = newNotification;
      resNotification.id = element.id;
      response.json(resNotification);
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: "Something went wrong" });
    });
};

exports.updateNotification = (request, response) => {
  console.log(request.body);
  const newNotification = {
    doctor_email: request.user.email ? request.user.email : "",
    patient_email: request.body.patientEmail ? request.body.patientEmail : "",
    text: request.body.text ? request.body.text : "",
    date_time: request.body.dateTime ? request.body.dateTime : "",
  };
  dataBase
    .doc(`/patient/${request.body.patientEmail}`)
    .get()
    .then((doc) => {
      if (doc.exists && doc.data().doctor_email === request.user.email) {
        console.log(request.params.notificationId);
        return dataBase
          .doc(`notifications/${request.params.notificationId}`)
          .update(newNotification);
      } else if (doc.exists && doc.data().doctor_email !== request.user.email) {
        return response.status(403).json({
          error: "Unauthorized to edit notifications to this patient.",
        });
      } else {
        return response.status(404).json({ error: "Patient not found." });
      }
    })
    .then(() => {
      return response.json({
        message: "Notification updated successfully.",
      });
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: "Something went wrong" });
    });
};

exports.deleteNotification = (request, response) => {
  const document = dataBase.doc(
    `/notifications/${request.params.notificationId}`
  );
  document
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return response.status(404).json({ error: "Notification not found" });
      }
      if (doc.data().doctor_email !== request.user.email) {
        return response
          .status(403)
          .json({ error: "Unauthorised to delete this notification" });
      } else {
        return document.delete();
      }
    })
    .then(() => {
      response.json({ message: "Notification deleted successfully" });
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};
