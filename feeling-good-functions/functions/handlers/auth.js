const { dataBase, admin } = require("../util/admin");

const config = require("../util/config");

const firebase = require("firebase");
firebase.initializeApp(config);

const {
  validateSignUpData,
  validateLogInData,
  reduceUserDetails,
} = require("../util/validators");

exports.signUp = (request, response) => {
  const newUser = {
    email: request.body.email,
    password: request.body.password,
    confirmPassword: request.body.confirmPassword,
    role: request.body.role.toLowerCase(),
  };

  const { valid, errors } = validateSignUpData(newUser);
  if (!valid) {
    return response.status(400).json(errors);
  }

  const noImg = "no-img.png";

  let token, userId;
  dataBase
    .doc(`/patient/${newUser.email}`)
    .get()
    .then((document) => {
      if (document.exists) {
        return response
          .status(400)
          .json({ email: "this email is already taken" });
      } else {
        return dataBase
          .doc(`/doctor/${newUser.email}`)
          .get()
          .then((document) => {
            if (document.exists) {
              return response
                .status(400)
                .json({ email: "this email is already taken" });
            } else {
              return firebase
                .auth()
                .createUserWithEmailAndPassword(
                  newUser.email,
                  newUser.password
                );
            }
          });
      }
    })
    .then((data) => {
      userId = data.user.uid;
      return data.user.getIdToken();
    })
    .then((idToken) => {
      token = idToken;
      const userCredentials = {
        email: newUser.email,
        createdAt: new Date().toISOString(),
        imageUrl: `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${noImg}?alt=media`,
        userId,
      };

      return newUser.role === "doctor"
        ? dataBase.doc(`/doctor/${newUser.email}`).set(userCredentials)
        : dataBase.doc(`/patient/${newUser.email}`).set(userCredentials);
    })
    .then(() => {
      return response.status(201).json({ token });
    })
    .catch((error) => {
      console.error(error);
      if (error.code === "auth/email-already-in-use") {
        return response.status(400).json({ email: "Email already in use" });
      } else {
        return response
          .status(500)
          .json({ general: "Something went wrong. Please try again." });
      }
    });
};

exports.logIn = (request, response) => {
  const user = {
    email: request.body.email,
    password: request.body.password,
  };

  const { valid, errors } = validateLogInData(user);
  if (!valid) {
    return response.status(400).json(errors);
  }

  firebase
    .auth()
    .signInWithEmailAndPassword(user.email, user.password)
    .then((data) => {
      return data.user.getIdToken();
    })
    .then((token) => {
      return response.json({ token });
    })
    .catch((error) => {
      console.error(error);
      return response
        .status(403)
        .json({ general: "Wrong credentials, please try again" });
    });
};

exports.getAuthenticatedUser = (request, response) => {
  let userData = {};
  dataBase
    .doc(`/patient/${request.user.email}`)
    .get()
    .then((doc) => {
      if (doc.exists) {
        userData.credentials = doc.data();
        return dataBase
          .collection("likes")
          .where("userEmail", "==", request.user.email)
          .get();
      } else {
        dataBase
          .doc(`/doctor/${request.user.email}`)
          .get()
          .then((doc) => {
            if (doc.exists) {
              userData.credentials = doc.data();
              return dataBase
                .collection("likes")
                .where("userEmail", "==", request.user.email)
                .get();
            }
          });
      }
    })
    .then((data) => {
      userData.likes = [];
      if (data && data.length) {
        data.forEach((doc) => {
          userData.likes.push(doc.data());
        });
      }
      return dataBase
        .collection("notifications")
        .where("recipient", "==", request.user.email)
        .orderBy("createdAt", "desc")
        .limit(10)
        .get();
    })
    .then((data) => {
      userData.notifications = [];
      if (data && data.length) {
        data.forEach((doc) => {
          userData.notifications.push({
            recipient: doc.data().recipient,
            sender: doc.data().sender,
            createdAt: doc.data().createdAt,
            screamId: doc.data().screamId,
            type: doc.data().type,
            read: doc.data().read,
            notificationId: doc.id,
          });
        });
      }
      return response.json(userData);
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};

exports.uploadImage = (request, response) => {
  const BusBoy = require("busboy");
  const path = require("path");
  const os = require("os");
  const fs = require("fs");

  const busBoy = new BusBoy({ headers: request.headers });
  let imageFileName,
    imageToBeUploaded = {};

  busBoy.on("file", (fieldName, file, fileName, encoding, mimeType) => {
    if (mimeType !== "image/jpeg" && mimeType !== "image/png") {
      return response.status(400).json({ error: "Wrong file type submitted" });
    }

    const imageExtention = fileName.split(".")[fileName.split(".").length - 1];

    imageFileName = `${Math.round(
      Math.random() * 10000000000
    )}.${imageExtention}`;

    const filePath = path.join(os.tmpdir(), imageFileName);
    imageToBeUploaded = { filePath, mimeType };
    file.pipe(fs.createWriteStream(filePath));
  });

  busBoy.on("finish", () => {
    admin
      .storage()
      .bucket()
      .upload(imageToBeUploaded.filePath, {
        resumable: false,
        metadata: {
          metadata: {
            contentType: imageToBeUploaded.mimeType,
          },
        },
      })
      .then(() => {
        const imageUrl = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${imageFileName}?alt=media`;
        return dataBase.doc(`/user/${request.user.email}`).update({ imageUrl });
      })
      .then(() => response.json({ message: "Image uploaded successfully" }))
      .catch((error) => {
        console.error(error);
        return response.status(500).json({ error: error.code });
      });
  });
  busBoy.end(request.rawBody);
};

exports.getUserDetails = (request, response) => {
  let userData = {};
  dataBase
    .doc(`/user/${request.params.email}`)
    .get()
    .then((doc) => {
      if (doc.exists) {
        userData.user = doc.data();
        return dataBase
          .collection("screams")
          .where("userEmail", "==", request.params.email)
          .orderBy("createdAt", "desc")
          .get();
      } else {
        return response.status(404).json({ error: "User not found" });
      }
    })
    .then((data) => {
      userData.screams = [];
      data.forEach((doc) => {
        userData.screams.push({
          body: doc.data().body,
          createdAt: doc.data().createdAt,
          userEmail: doc.data().userEmail,
          userImage: doc.data().userImage,
          likeCount: doc.data().likeCount,
          commentCount: doc.data().commentCount,
          screamId: doc.id,
        });
      });
      return response.json(userData);
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};
exports.markNotificationsRead = (request, response) => {
  let batch = dataBase.batch();
  request.body.forEach((notificationId) => {
    const notification = dataBase.doc(`/notifications/${notificationId}`);
    batch.update(notification, { read: true });
  });
  batch
    .commit()
    .then(() => {
      return response.json({ message: "Notifications marked read" });
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};
