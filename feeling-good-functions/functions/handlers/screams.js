const { dataBase } = require("../util/admin");

exports.getAllScreams = (request, response) => {
  dataBase
    .collection("screams")
    .orderBy("createdAt", "desc")
    .get()
    .then(data => {
      let screams = [];
      data.forEach(element => {
        screams.push({
          screamId: element.id,
          body: element.data().body,
          userHandle: element.data().userHandle,
          createdAt: element.data().createdAt,
          commentCount: element.data().commentCount,
          likeCount: element.data().likeCount,
          userImage: element.data().userImage
        });
      });
      return response.json(screams);
    })
    .catch(error => console.error(err));
};

exports.postOneScream = (request, response) => {
  const newScream = {
    body: request.body.body,
    userHandle: request.user.handle,
    userImage: request.user.imageUrl,
    createdAt: new Date().toISOString(),
    likeCount: 0,
    commentCount: 0
  };

  dataBase
    .collection("screams")
    .add(newScream)
    .then(element => {
      const resScream = newScream;
      resScream.screamId = element.id;
      response.json(resScream);
    })
    .catch(error => {
      console.error(error);
      return response.status(500).json({ error: "something went wrong" });
    });
};

exports.getScream = (request, response) => {
  let screamData = {};
  dataBase
    .doc(`/screams/${request.params.screamId}`)
    .get()
    .then(doc => {
      if (!doc.exists) {
        return response.status(404).json({ error: "Scream not found" });
      }
      screamData = doc.data();
      screamData.screamId = doc.id;
      return dataBase
        .collection("comments")
        .orderBy("createdAt", "desc")
        .where("screamId", "==", request.params.screamId)
        .get();
    })
    .then(data => {
      screamData.comments = [];
      data.forEach(doc => screamData.comments.push(doc.data()));
      return response.json(screamData);
    })
    .catch(error => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};

exports.commentOnScream = (request, response) => {
  if (request.body.body.trim() === "") {
    return response.status(400).json({ error: "Must not be empty" });
  }
  const newComment = {
    body: request.body.body,
    createdAt: new Date().toISOString(),
    screamId: request.params.screamId,
    userHandle: request.user.handle,
    userImage: request.user.imageUrl
  };

  dataBase
    .doc(`/screams/${request.params.screamId}`)
    .get()
    .then(doc => {
      if (!doc.exists) {
        return response.status(404).json({ error: "Scream not found" });
      }
      return doc.ref.update({ commentCount: doc.data().commentCount + 1 });
    })
    .then(() => {
      return dataBase.collection("comments").add(newComment);
    })
    .then(() => {
      response.json(newComment);
    })
    .catch(error => {
      console.error(error);
      return response.status(500).json({ error: "Something went wrong" });
    });
};

exports.unlikeScream = (request, response) => {
  const likeDocument = dataBase
    .collection("likes")
    .where("userHandle", "==", request.user.handle)
    .where("screamId", "==", request.params.screamId)
    .limit(1);

  const screamDocument = dataBase.doc(`/screams/${request.params.screamId}`);

  let screamData = {};

  screamDocument
    .get()
    .then(doc => {
      if (doc.exists) {
        screamData = doc.data();
        screamData.screamId = doc.id;
        return likeDocument.get();
      } else {
        return response.status(404).json({ error: "Scream not found" });
      }
    })
    .then(data => {
      if (data.empty) {
        return response.status(400).json({ error: "Scream not liked" });
      } else {
        return dataBase
          .doc(`/likes/${data.docs[0].id}`)
          .delete()
          .then(() => {
            screamData.likeCount--;
            return screamDocument.update({ likeCount: screamData.likeCount });
          })
          .then(() => {
            response.json(screamData);
          });
      }
    })
    .catch(error => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};

exports.likeScream = (request, response) => {
  const likeDocument = dataBase
    .collection("likes")
    .where("userHandle", "==", request.user.handle)
    .where("screamId", "==", request.params.screamId)
    .limit(1);

  const screamDocument = dataBase.doc(`/screams/${request.params.screamId}`);

  let screamData = {};

  screamDocument
    .get()
    .then(doc => {
      if (doc.exists) {
        screamData = doc.data();
        screamData.screamId = doc.id;
        return likeDocument.get();
      } else {
        return response.status(404).json({ error: "Scream not found" });
      }
    })
    .then(data => {
      if (data.empty) {
        return dataBase
          .collection("likes")
          .add({
            screamId: request.params.screamId,
            userHandle: request.user.handle
          })
          .then(() => {
            screamData.likeCount++;
            return screamDocument.update({
              likeCount: screamData.likeCount
            });
          })
          .then(() => {
            return response.json(screamData);
          });
      } else {
        return response.status(400).json({ error: "Scream already liked" });
      }
    })
    .catch(error => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};

exports.deleteScream = (request, response) => {
  const document = dataBase.doc(`/screams/${request.params.screamId}`);
  document
    .get()
    .then(doc => {
      if (!doc.exists) {
        return response.status(404).json({ error: "Scream not found" });
      }
      if (doc.data().userHandle !== request.user.handle) {
        return response.status(403).json({ error: "Unauthorised" });
      } else {
        return document.delete();
      }
    })
    .then(() => {
      response.json({ message: "Scream deleted successfully" });
    })
    .catch(error => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};
