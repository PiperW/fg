const { dataBase } = require("../util/admin");

exports.getAppointmentsForPatient = (request, response) => {
  dataBase
    .doc(`/patient/${request.params.patientEmail}`)
    .get()
    .then((doc) => {
      if (doc.exists) {
        return dataBase
          .collection("appointments")
          .where("patient_email", "==", request.params.patientEmail)
          .get();
      } else {
        return response.status(404).json({ error: "Patient not found." });
      }
    })
    .then((data) => {
      const appointments = [];
      data.forEach((doc) => {
        appointments.push({
          id: doc.id,
          doctorEmail: doc.data().doctor_email,
          patientEmail: doc.data().patient_email,
          reason: doc.data().reason,
          simptoms: doc.data().simptoms,
          diagnostics: doc.data().diagnostics,
          treatment: doc.data().treatment,
          trimitereId: doc.data().trimitere_id,
          dateTime: doc.data().date_time,
        });
      });
      return response.json(appointments);
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};

exports.addAppointment = (request, response) => {
  const newAppointment = {
    doctor_email: request.user.email ? request.user.email : "",
    patient_email: request.body.patientEmail ? request.body.patientEmail : "",
    reason: request.body.reason ? request.body.reason : "",
    simptoms: request.body.simptoms ? request.body.simptoms : "",
    diagnostics: request.body.diagnostics ? request.body.diagnostics : "",
    treatment: request.body.treatment ? request.body.treatment : "",
    trimitere_id: request.body.trimitereId ? request.body.trimitereId : "",
    date_time: request.body.dateTime
      ? new Date(request.body.dateTime).toISOString()
      : "",
  };
  console.log(request.body);
  dataBase
    .doc(`/patient/${request.body.patientEmail}`)
    .get()
    .then((doc) => {
      if (doc.exists && doc.data().doctor_email === request.user.email) {
        console.log(request.body);
        return dataBase.collection("appointments").add(newAppointment);
      } else if (doc.exists && doc.data().doctor_email !== request.user.email) {
        return response
          .status(403)
          .json({ error: "Unauthorized to add appointments to this patient." });
      } else {
        return response.status(404).json({ error: "Patient not found." });
      }
    })
    .then((element) => {
      const resAppointment = newAppointment;
      resAppointment.id = element.id;
      response.json(resAppointment);
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: "Something went wrong" });
    });
};

exports.updateAppointment = (request, response) => {
  console.log(request.body);
  const newAppointment = {
    doctor_email: request.user.email ? request.user.email : "",
    patient_email: request.body.patientEmail ? request.body.patientEmail : "",
    reason: request.body.reason ? request.body.reason : "",
    simptoms: request.body.simptoms ? request.body.simptoms : "",
    diagnostics: request.body.diagnostics ? request.body.diagnostics : "",
    treatment: request.body.treatment ? request.body.treatment : "",
    trimitere_id: request.body.trimitereId ? request.body.trimitereId : "",
    date_time: request.body.dateTime
      ? new Date(request.body.dateTime).toISOString()
      : "",
  };
  dataBase
    .doc(`/patient/${request.body.patientEmail}`)
    .get()
    .then((doc) => {
      if (doc.exists && doc.data().doctor_email === request.user.email) {
        console.log(request.params.appointmentId);
        return dataBase
          .doc(`appointments/${request.params.appointmentId}`)
          .update(newAppointment);
      } else if (doc.exists && doc.data().doctor_email !== request.user.email) {
        return response.status(403).json({
          error: "Unauthorized to edit appointments to this patient.",
        });
      } else {
        return response.status(404).json({ error: "Patient not found." });
      }
    })
    .then(() => {
      return response.json({
        message: "Appointment updated successfully.",
      });
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: "Something went wrong" });
    });
};

exports.deleteAppointment = (request, response) => {
  const document = dataBase.doc(
    `/appointments/${request.params.appointmentId}`
  );
  document
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return response.status(404).json({ error: "Appointment not found" });
      }
      if (doc.data().doctor_email !== request.user.email) {
        return response
          .status(403)
          .json({ error: "Unauthorised to delete this appointment" });
      } else {
        return document.delete();
      }
    })
    .then(() => {
      response.json({ message: "Appointment deleted successfully" });
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};
