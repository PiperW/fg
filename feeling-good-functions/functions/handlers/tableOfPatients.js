const { dataBase } = require("../util/admin");
const {
  reduceUserDetails,
  isValidID,
  getValidBloodType,
} = require("../util/validators");

exports.getPatientsForDoctor = (request, response) => {
  dataBase
    .doc(`/doctor/${request.params.doctorEmail}`)
    .get()
    .then((doc) => {
      if (doc.exists) {
        return dataBase
          .collection("patient")
          .where("doctor_email", "==", request.params.doctorEmail)
          .get();
      } else {
        return response.status(404).json({ error: "Doctor not found." });
      }
    })
    .then((data) => {
      const patients = [];
      data.forEach((doc) => {
        patients.push({
          email: doc.data().email,
          fullName: doc.data().full_name,
          cnp: doc.data().cnp,
          birthday: doc.data().birthday,
          address: doc.data().address,
          bloodType: doc.data().blood_type,
          allergies: doc.data().allergies,
          affections: doc.data().affections,
        });
      });
      return response.json(patients);
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};

exports.addPatientDetails = (request, response) => {
  let userDetails = reduceUserDetails(request.body, request.user.email);
  console.log(request.body);
  console.log(request.user.email);
  dataBase
    .doc(`/patient/${request.body.email}`)
    .get()
    .then((doc) => {
      if (
        doc.exists &&
        (doc.data().doctor_email === undefined ||
          doc.data().doctor_email === request.user.email)
      ) {
        const validId = isValidID(request.body.cnp),
          bloodType = getValidBloodType(request.body.bloodType);
        console.log(validId);
        console.log(bloodType);
        if (validId && bloodType !== "") {
          console.log("1");
          return dataBase
            .doc(`patient/${request.body.email}`)
            .update(userDetails)
            .then(() => {
              return response.json({
                message: "Patient info added successfully.",
              });
            });
        } else {
          console.log("2");
          const errorMessage =
            (!validId ? "Invalid ID. " : "") +
            (bloodType === "" ? "Invalid blood type." : "");
          return response.status(400).json({ error: errorMessage });
        }
      } else if (doc.exists && doc.data().doctor_email !== request.user.email) {
        console.log("3");
        return response
          .status(400)
          .json({ error: "Patient already has a doctor defined." });
      } else {
        console.log("4");
        return response.status(404).json({ error: "Patient not found." });
      }
    })
    .catch((error) => {
      console.log("5");
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};

exports.deletePatient = (request, response) => {
  const document = dataBase.doc(`/patient/${request.params.patientEmail}`);
  console.log(request.params.patientEmail);
  document
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return response.status(404).json({ error: "Patient not found" });
      }
      if (doc.data().doctor_email !== request.user.email) {
        return response.status(403).json({ error: "Unauthorised" });
      } else {
        return document.delete();
      }
    })
    .then(() => {
      response.json({ message: "User deleted successfully" });
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};
