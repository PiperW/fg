const { dataBase } = require("../util/admin");

exports.getAlarms = (request, response) => {
  dataBase
    .collection("alarme")
    .where("email_doctor", "==", request.user.email)
    .get()
    .then((data) => {
      const alarms = [];
      data.forEach((doc) => {
        const alarmObj = {
          id: doc.id,
          createdAt: doc.data().data,
          text: doc.data().descriere,
          patientEmail: doc.data().email,
          doctorEmail: doc.data().email_doctor,
          status: doc.data().status,
          title: doc.data().titlu,
        };
        if (alarmObj.status === "OPEN") {
          alarms.push(alarmObj);
        }
      });
      return response.json(alarms);
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};

exports.resolveAlarm = (request, response) => {
  const newAlarm = {
    data: request.body.createdAt,
    descriere: request.body.text,
    email: request.body.patientEmail,
    email_doctor: request.body.doctorEmail,
    status: "RESOLVED",
    titlu: request.body.title,
  };
  const document = dataBase.doc(`/alarme/${request.params.alarmId}`);
  document
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return response.status(404).json({ error: "Alarm not found" });
      }
      if (doc.data().email_doctor !== request.user.email) {
        return response
          .status(403)
          .json({ error: "Unauthorised to resolve this Alarm" });
      } else {
        return document.update(newAlarm);
      }
    })
    .then(() => {
      return response.json({ message: "Alarm resolved successfully" });
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};
