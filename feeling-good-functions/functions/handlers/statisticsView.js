const moment = require("moment");
const { dataBase } = require("../util/admin");

exports.getStats = (request, response) => {
  dataBase
    .collection("masuratori")
    .orderBy("data", "asc")
    .where("email", "==", request.params.patientEmail)
    .limit(10000)
    .get()
    .then((data) => {
      const ecg = [],
        temperature = [],
        humidity = [];
      data.forEach((doc) => {
        const date = moment(doc.data().data).valueOf();
        console.log(date);
        const ecgObj = [date, parseInt(doc.data().ecg)],
          temperatureObj = [date, parseFloat(doc.data().temperatura)],
          humidityObj = [date, parseFloat(doc.data().umiditate)];
        if (ecgObj[1] != 0) {
          ecg.push(ecgObj);
        }
        if (temperatureObj[1] != 0) {
          temperature.push(temperatureObj);
        }
        if (humidityObj[1] != 0) {
          humidity.push(humidityObj);
        }
      });
      return response.json({
        ecg: {
          name: "ECG",
          columns: ["time", "value"],
          points: ecg.length ? ecg : [[0, 0]],
        },
        temperature: {
          name: "Temperature",
          columns: ["time", "value"],
          points: temperature.length ? temperature : [[0, 0]],
        },
        humidity: {
          name: "Humidity",
          columns: ["time", "value"],
          points: humidity.length ? humidity : [[0, 0]],
        },
      });
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ error: error.code });
    });
};

exports.getNormalValues = (request, response) => {
  dataBase
    .collection("setari_parametrii_pacient")
    .where("email", "==", request.params.patientEmail)
    .limit(1)
    .get()
    .then((data) => {
      let res = {};
      if (!data.empty) {
        data.forEach((doc) => {
          res = {
            id: doc.id,
            email: doc.data().email,
            maxEcg: parseInt(doc.data().max_ecg),
            maxTemperature: parseFloat(doc.data().max_temperatura),
            maxHumidity: parseFloat(doc.data().max_umiditate),
            minEcg: parseInt(doc.data().min_ecg),
            minTemperature: parseFloat(doc.data().min_temperatura),
            minHumidity: parseFloat(doc.data().min_umiditate),
          };
        });
        return response.json(res);
      }
      return response.status(404).json({
        general:
          "No parameter settings found. Please set the normal values for the selected patient's parameters.",
      });
    })
    .catch((error) => {
      console.error(error);
      return response
        .status(500)
        .json({ general: "Something went wrong. Please try again." });
    });
};

exports.saveNormalValues = (request, response) => {
  if (
    request.body.minEcg > request.body.maxEcg ||
    request.body.minTemperature > request.body.maxTemperature ||
    request.body.minHumidity > request.body.maxHumidity
  ) {
    return response
      .status(400)
      .json({ error: "Min value is greater than max value." });
  }

  const newObj = {
    max_ecg: request.body.maxEcg ? request.body.maxEcg.toString() : "0",
    max_temperatura: request.body.maxTemperature
      ? request.body.maxTemperature.toString()
      : "0",
    max_umiditate: request.body.maxHumidity
      ? request.body.maxHumidity.toString()
      : "0",
    min_ecg: request.body.minEcg ? request.body.minEcg.toString() : "0",
    min_temperatura: request.body.minTemperature
      ? request.body.minTemperature.toString()
      : "0",
    min_umiditate: request.body.minHumidity
      ? request.body.minHumidity.toString()
      : "0",
    email: request.body.email ? request.body.email : "",
  };
  dataBase
    .doc(`/patient/${request.body.email}`)
    .get()
    .then((doc) => {
      if (doc.exists) {
        return dataBase
          .doc(`/setari_parametrii_pacient/${request.body.id}`)
          .get()
          .then((doc) => {
            if (!doc.exists) {
              return dataBase
                .collection("setari_parametrii_pacient")
                .add(newObj);
            } else {
              return dataBase
                .doc(`/setari_parametrii_pacient/${request.body.id}`)
                .update(newObj);
            }
          })
          .then((element) => {
            if (element) {
              const res = newObj;
              res.id = element.id;
              return response.json(res);
            }
          });
      } else {
        return response.status(404).json({ error: "Patient not found." });
      }
    })
    .catch((error) => {
      console.error(error);
      return response
        .status(500)
        .json({ error: "Something went wrong. Please try again." });
    });
};
