const isEmpty = (string) => {
  return string.trim() === "";
};

const isEmail = (email) => {
  const regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return email.match(regEx);
};

exports.validateSignUpData = (data) => {
  let errors = {};

  if (isEmpty(data.email)) {
    errors.email = "Must not be empty";
  } else if (!isEmail(data.email)) {
    errors.email = "Must be avalid email address";
  }

  if (isEmpty(data.password)) {
    errors.password = "Must not be empty";
  }

  if (data.password !== data.confirmPassword) {
    errors.confirmPassword = "Passwords must match";
  }

  if (
    data.role.toLowerCase() !== "doctor" &&
    data.role.toLowerCase() !== "patient"
  ) {
    errors.role = "Invalid role";
  }

  return {
    errors,
    valid: Object.keys(errors).length === 0,
  };
};

exports.validateLogInData = (data) => {
  let errors = {};

  if (isEmpty(data.email)) {
    errors.email = "Must not be empty";
  }
  if (isEmpty(data.password)) {
    errors.password = "Must not be empty";
  }

  return {
    errors,
    valid: Object.keys(errors).length === 0,
  };
};

exports.getValidBloodType = (bloodTypeString) => {
  bloodTypeString = bloodTypeString ? bloodTypeString.toUpperCase() : "";
  const possibleBloodTypes = [
      "A+",
      "A-",
      "B+",
      "B-",
      "O+",
      "O-",
      "AB+",
      "AB-",
      "A",
      "B",
      "O",
      "AB",
    ],
    matchingBloodType = possibleBloodTypes.find((e) => e === bloodTypeString);
  if (matchingBloodType) {
    return matchingBloodType;
  } else {
    return "";
  }
};

exports.reduceUserDetails = (data, doctorEmail) => {
  return {
    doctor_email: doctorEmail,
    full_name:
      data.fullName && !isEmpty(data.fullName) ? data.fullName.trim() : "",
    cnp: data.cnp && exports.isValidID(data.cnp) ? data.cnp.trim() : "",
    birthday:
      data.birthday && !isEmpty(data.birthday)
        ? new Date(data.birthday).toISOString()
        : "",
    address: data.address && !isEmpty(data.address) ? data.address.trim() : "",
    blood_type:
      data.bloodType && !isEmpty(data.bloodType)
        ? exports.getValidBloodType(data.bloodType)
        : "",
    allergies:
      data.allergies && !isEmpty(data.allergies) ? data.allergies.trim() : "",
    affections:
      data.affections && !isEmpty(data.affections)
        ? data.affections.trim()
        : "",
  };
};

exports.isValidID = (cnp) => {
  let msg = "",
    c = [];

  if (cnp.trim().length != 13) {
    return false;
  }

  if (cnp.trim() == "0000000000000") {
    return false;
  }

  for (let i = 0; i <= 12; i++) {
    c.push(cnp.charAt(i));
  }

  let sum =
    c[0] * 2 +
    c[1] * 7 +
    c[2] * 9 +
    c[3] * 1 +
    c[4] * 4 +
    c[5] * 6 +
    c[6] * 3 +
    c[7] * 5 +
    c[8] * 8 +
    c[9] * 2 +
    c[10] * 7 +
    c[11] * 9;
  let control = sum % 11;

  if (control == 10) {
    control = 1;
  }

  if (control != c[12]) {
    return false;
  }

  return true;
};
