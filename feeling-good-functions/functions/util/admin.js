const admin = require("firebase-admin");
const serviceAccount = require("../ServiceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const dataBase = admin.firestore();

module.exports = { admin, dataBase };
